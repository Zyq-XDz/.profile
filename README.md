[![Typing SVG](https://readme-typing-svg.herokuapp.com?font=Roboto&weight=900&pause=1000&color=000000&random=true&width=435&lines=Hi+there+%F0%9F%91%8B)](https://git.io/typing-svg)

# Github
[XDzZyq](https://github.com/XDzzzzzZyq)

# Contacts
[**Chat**](https://blender.chat/direct/xdzzyq): XDzZyq

[**Email**](xiaodouzizyq@gmail.com): xiaodouzizyq@gmail.com

[**Twitter**](https://twitter.com/zyq30905782): https://twitter.com/zyq30905782

[**Bilibili**](https://space.bilibili.com/302469604): https://space.bilibili.com/302469604

# Reports
## [**2024**](https://projects.blender.org/Zyq-XDz/.profile/src/branch/main/reports/2024.md)
